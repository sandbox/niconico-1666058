<?php
/**
 * @file
 * Administrative page callbacks for the Country Redirect module.
 */

/**
 * country_redirect administration settings.
 *
 * @return
 *   Form for country_redirect configuration options.
 */
function country_redirect_ipinfodb_settings($form, &$form_state) {
  $form['country_redirect_ipinfodb_apikey'] = array(
      '#title' => t('IPinfoDB.com API Key'),
      '#type' => 'textfield',
      '#size' => 100,
      '#description' => t('Please provide a valid IPInfoDB.com API key. If you don\'t have one, you can register at <a href="@ipinfo">IPInfoDB.com</a>.', array('@ipinfo' => url('http://www.ipinfodb.com/register.php'))),
      '#default_value' => variable_get('country_redirect_ipinfodb_apikey', '0'),
      '#required' => TRUE
  );

  $form['country_redirect_url'] = array(
      '#title' => t("Path"),
      '#type' => 'textfield',
      '#description' => t("Le chemin de la page pour laquelle doit se faire la redirection"),
      '#default_value' => variable_get('country_redirect_url', ''),
      '#required' => TRUE
  );

  return system_settings_form($form);
}


/**
 * Menu callback. Display blocked IP addresses.
 *
 * @param $default_ip
 *   Optional IP address to be passed on to drupal_get_form() for
 *   use as the default value of the IP address form field.
 */
function country_redirect_redirects_settings($default_ip = '') {
  $rows = array();
  $header = array(t('Country code'), t('URL'), t('Operations'));
  $result = db_query('SELECT rid, country_code, url FROM {country_redirect}');
  foreach ($result as $redirect) {
    $rows[] = array(
        $redirect->country_code,
        $redirect->url,
        l(t('delete'), "admin/config/system/country_redirect/delete/$redirect->rid"),
    );
  }

  $build['country_redirect_form'] = drupal_get_form('country_redirect_redirects', $default_ip);

  $build['country_redirect_table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t("There's no redirection."),
  );

  return $build;
}

/**
 * Define the form for redirects.
 *
 * @ingroup forms
 * @see country_redirect_form_validate()
 * @see country_redirect_form_submit()
 */
function country_redirect_redirects($form, $form_state) {
  $form['country'] = array(
      '#title' => t('Country code'),
      '#type' => 'textfield',
      '#size' => 2,
      '#maxlength' => 2,
      '#default_value' => '',
      '#description' => t('The 2 letters ISO 3166-1 country code. This code can be found in <a href="https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements">the list of ISO 3166 codes on Wikipedia</a>'),
  );
  $form['url'] = array(
      '#title' => t('URL to redirect to'),
      '#type' => 'textfield',
      '#default_value' => '',
      '#description' => t("The URL the user will be redirect to. This can be an internal Drupal path such as %add-node or an external URL such as %drupal.", array('%add-node' => 'node/add', '%drupal' => 'http://drupal.org')),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add'),
  );
  $form['#submit'][] = 'country_redirect_form_submit';
  $form['#validate'][] = 'country_redirect_form_validate';
  return $form;
}

function country_redirect_form_validate($form, &$form_state) {
  $cc = trim($form_state['values']['country']);
  $url = trim($form_state['values']['url']);
  if (db_query("SELECT * FROM {country_redirect} WHERE country_code = :cc AND url = :url", array(':cc' => $cc, ':url' => $url))->fetchAssoc()) {
    form_set_error('url', t('This redirection already exists.'));
  }
}

function country_redirect_form_submit($form, &$form_state) {
  $cc = strtoupper(trim($form_state['values']['country']));
  $url = trim($form_state['values']['url']);
  db_insert('country_redirect')
  ->fields(array('country_code' => $cc, 'url' => $url))
  ->execute();
  drupal_set_message(t('The redirection have been added.'));
  $form_state['redirect'] = 'admin/config/system/country_redirect/redirects';
  return;
}

/**
 * IP deletion confirm page.
 *
 * @see country_redirect_delete_submit(),
 */
function country_redirect_delete($form, &$form_state, $rid) {
  $form['redirect_id'] = array(
      '#type' => 'value',
      '#value' => $rid,
  );
  return confirm_form($form, t('Are you sure you want to delete this redirection ?'), 'admin/config/system/country_redirect/redirects', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Process country_redirect_delete form submissions.
 */
function country_redirect_delete_submit($form, &$form_state) {
  $redirect_id = $form_state['values']['redirect_id']->rid;
  db_delete('country_redirect')
  ->condition('rid', $redirect_id)
  ->execute();
  watchdog('country_redirect', 'Deleted redirection %rid', array('%rid' => $redirect_id));
  drupal_set_message(t('The redirection was deleted.'));
  $form_state['redirect'] = 'admin/config/system/country_redirect/redirects';
}
